const fileCacheName = 'od-v1';

self.addEventListener('install', function (event) {
  event.waitUntil(
    caches.open(fileCacheName).then(function (cache) {
      return cache.addAll([
        '/',
        '/index.html',
        '/favicon.ico',
        '/inline.bundle.js',
        '/polyfills.bundle.js',
        '/scripts.bundle.js',
        '/styles.bundle.js',
        '/main.bundle.js'

        // etc
      ]);
    })
  );
});

self.addEventListener('activate', function (event) {
  event.waitUntil(
    caches.keys().then(function (cacheNames) {
      return Promise.all(
        cacheNames.filter(function (cacheName) {
          // Return true if you want to remove this cache,
          // but remember that caches are shared across
          // the whole origin
        }).map(function (cacheName) {
          return caches.delete(cacheName);
        })
      );
    })
  );
});

self.addEventListener('fetch', function (event) {
  event.respondWith(
    caches.open(fileCacheName).then(function (cache) {
      return cache.match(event.request).then(function (response) {
        var fetchPromise = fetch(event.request).then(function (networkResponse) {
          cache.put(event.request, networkResponse.clone());
          return networkResponse;
        })
        return response || fetchPromise;
      })
    })
  );
});
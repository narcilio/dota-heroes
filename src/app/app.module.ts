import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { MenuComponent } from './menu/menu.component';
import { ListComponent } from './list/list.component';
import { HeroBlockComponent } from './hero-block/hero-block.component';
import { SearchPipe } from './list/search-filter';
import { SortPipe } from './list/sort-filter';
import { HeroDetailComponent } from './hero-detail/hero-detail.component';
import { LazyLoadImagesModule } from 'ngx-lazy-load-images';

const appRoutes: Routes = [
  { path: 'hero/:id', component: HeroDetailComponent, data: { state: 'detail' }},
  { path: 'heroes', component: ListComponent, data: { state: 'list' }},
  { path: '', redirectTo: '/heroes', pathMatch: 'full'}
];

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    ListComponent,
    HeroBlockComponent,
    SearchPipe,
    SortPipe,
    HeroDetailComponent
  ],
  imports: [
    BrowserAnimationsModule,
    LazyLoadImagesModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

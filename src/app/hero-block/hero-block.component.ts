import { Component, OnInit, Input, Output} from '@angular/core';
import { GlobalVariables } from '../app.globals';



@Component({
  selector: 'app-hero-block',
  templateUrl: './hero-block.component.html',
  styleUrls: ['./hero-block.component.css']
})
export class HeroBlockComponent implements OnInit {
  @Input() hero: any;

  constructor(private globals: GlobalVariables) { }

  ngOnInit() { }

  heroImg() {
    return 'https://api.opendota.com' + this.hero.img;
  }

  defaultImg(){
    return 'https://via.placeholder.com/175x233/cccccc/cccccc';
  }

}

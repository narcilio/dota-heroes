import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

@Injectable()
export class HeroService {

  defaultHeaders = new HttpHeaders();

  constructor(private http: HttpClient) {
    this.defaultHeaders.append('Content-Type', 'application/json');
    this.defaultHeaders.append('Access-Control-Allow-Origin', '*');
    this.defaultHeaders.append('Access-Control-Allow-Headers', '*');
  }

  public getHeroes(): Observable<Array<any>> {
    return this.http.get<Array<any>>('https://api.opendota.com/api/heroStats',
      { headers: this.defaultHeaders });
  }

  public getHero(id: number): Observable<any> {
    return this.getHeroes().map(heroes => heroes.find(hero => hero.id === id));
  }

  public getBenchmarks(id: number): Observable<any> {
    return this.http.get<any>('https://api.opendota.com/api/benchmarks?hero_id=' + id, { headers: this.defaultHeaders });
  }

  public getDurations(id: number): Observable<any> {
    return this.http.get<any>('https://api.opendota.com/api/heroes/' + id + '/durations', { headers: this.defaultHeaders });
  }
}

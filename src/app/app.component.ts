import { Component, OnInit } from '@angular/core';
import { MenuComponent } from './menu/menu.component';
import { Router, NavigationEnd } from '@angular/router';
import { Observable, Subscriber, Subject } from 'rxjs';
import { trigger, animate, style, group, animateChild, query, stagger, transition, state } from '@angular/animations';
import { GlobalVariables} from './app.globals';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [GlobalVariables],
  animations: [trigger('routerTransition', [
    transition('list => detail', [
      query(':enter, :leave', [
        style({})
      ], { optional: true }),
      group([
        query(':enter', [
          style({ opacity: 0, 'margin-top': '30%' }),
          animate('.5s', style({ opacity: 1, 'margin-top': '0' }))
        ], { optional: true })
      ])
    ])
  ])]
})
export class AppComponent implements OnInit {

  searching = false;

  constructor(private router: Router, private globals: GlobalVariables) { 
  }


  ngOnInit() {
    this.router.events.subscribe((evt) => {
      if (!(evt instanceof NavigationEnd)) {
        return;
      }
      window.scrollTo(0, 0);
      this.searching = false;
    });
  }

  getRouteAnimation(outlet) {
    return outlet.activatedRouteData.state;
  }

  getCurrentUrl(){
    return this.router.url;
  }
}

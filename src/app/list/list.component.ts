import { Component, OnInit, Input } from '@angular/core';
import { trigger, style, transition, animate } from '@angular/animations';
import { HeroService } from '../services/hero.service';
import { GlobalVariables } from '../app.globals';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  providers: [HeroService],
  animations: [trigger('fadeInTopAnimation', [
    // route 'enter' transition
    transition(':enter', [

      // styles at start of transition
      style({ opacity: 0, 'margin-top': '30%' }),

      // animation and styles at end of transition
      animate('.5s', style({ opacity: 1, 'margin-top': '0' }))
    ]),
    transition(':leave', [

      // styles at start of transition
      style({ transform: 'scale(1)' }),

      // animation and styles at end of transition
      animate('.3s', style({ transform: 'scale(0)' }))
    ])
  ])]
})
export class ListComponent implements OnInit {

  //@Input() query: string;
  heroes: Array<any>;

  constructor(private service: HeroService, private globals: GlobalVariables) { }

  ngOnInit() {

    this.service.getHeroes().subscribe(heroes => {
      this.heroes = heroes;
    });
  }
}

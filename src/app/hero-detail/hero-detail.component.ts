import { Component, OnInit, Input, Output, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { HeroService } from '../services/hero.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-hero-detail',
  templateUrl: './hero-detail.component.html',
  styleUrls: ['./hero-detail.component.css'],
  providers: [HeroService],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeroDetailComponent implements OnInit {

  @Input() heroId: number;
  hero: any;
  match_pro_total = 0;

  @Output()
  pro_picks_perc = 0;
  @Output()
  pro_bans_perc = 0;
  @Output()
  pro_wins_perc = 0;

  @Output()
  divine_picks_perc = 0;
  @Output()
  divine_wins_perc = 0;
  @Output()
  ancient_picks_perc = 0;
  @Output()
  ancient_wins_perc = 0;
  @Output()
  legend_picks_perc = 0;
  @Output()
  legend_wins_perc = 0;
  @Output()
  archon_picks_perc = 0;
  @Output()
  archon_wins_perc = 0;
  @Output()
  crusader_picks_perc = 0;
  @Output()
  crusader_wins_perc = 0;
  @Output()
  guardian_picks_perc = 0;
  @Output()
  guardian_wins_perc = 0;
  @Output()
  herald_picks_perc = 0;
  @Output()
  herald_wins_perc = 0;
  @Output()
  benchmarks;
  @Output()
  durations;

  constructor(private service: HeroService, private route: ActivatedRoute, private ref: ChangeDetectorRef) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => this.service.getHero(+params.get('id')).subscribe(hero => {
      this.hero = hero;
      this.service.getHeroes().subscribe(heroes => {
        this.match_pro_total = heroes.map(h => h.pro_pick || 0
        ).reduce((a, b) => a + b) / 10;
        this.pro_picks_perc = (this.hero.pro_pick / this.match_pro_total) * 100;
        this.pro_bans_perc = (this.hero.pro_ban / this.match_pro_total) * 100;
        this.pro_wins_perc = (this.hero.pro_win / this.hero.pro_pick) * 100;

        const match_7_total = heroes.map(h => h['7_pick'] || 0).reduce((a, b) => a + b) / 10;
        const match_6_total = heroes.map(h => h['6_pick'] || 0).reduce((a, b) => a + b) / 10;
        const match_5_total = heroes.map(h => h['5_pick'] || 0).reduce((a, b) => a + b) / 10;
        const match_4_total = heroes.map(h => h['4_pick'] || 0).reduce((a, b) => a + b) / 10;
        const match_3_total = heroes.map(h => h['3_pick'] || 0).reduce((a, b) => a + b) / 10;
        const match_2_total = heroes.map(h => h['2_pick'] || 0).reduce((a, b) => a + b) / 10;
        const match_1_total = heroes.map(h => h['1_pick'] || 0).reduce((a, b) => a + b) / 10;

        this.divine_picks_perc = (this.hero['7_pick'] / match_7_total) * 100;
        this.divine_wins_perc = (this.hero['7_win'] / this.hero['7_pick']) * 100;

        this.ancient_picks_perc = (this.hero['6_pick'] / match_6_total) * 100;
        this.ancient_wins_perc = (this.hero['6_win'] / this.hero['6_pick']) * 100;

        this.legend_picks_perc = (this.hero['5_pick'] / match_5_total) * 100;
        this.legend_wins_perc = (this.hero['5_win'] / this.hero['5_pick']) * 100;

        this.archon_picks_perc = (this.hero['4_pick'] / match_4_total) * 100;
        this.archon_wins_perc = (this.hero['4_win'] / this.hero['4_pick']) * 100;

        this.crusader_picks_perc = (this.hero['3_pick'] / match_3_total) * 100;
        this.crusader_wins_perc = (this.hero['3_win'] / this.hero['3_pick']) * 100;

        this.guardian_picks_perc = (this.hero['2_pick'] / match_2_total) * 100;
        this.guardian_wins_perc = (this.hero['2_win'] / this.hero['2_pick']) * 100;

        this.herald_picks_perc = (this.hero['1_pick'] / match_1_total) * 100;
        this.herald_wins_perc = (this.hero['1_win'] / this.hero['1_pick']) * 100;

        this.service.getBenchmarks(hero.id).subscribe(benchmarks => {
          this.benchmarks = benchmarks.result;
          this.ref.detectChanges();
        });

        this.service.getDurations(hero.id).subscribe(durations => {
          this.durations = durations.map(item => ({
            win: item.wins,
            games: item.games_played,
            x: (item.duration_bin / 60),
          })).sort((a, b) => a.x - b.x);
          this.ref.detectChanges();
        });

        this.ref.detectChanges();
      });
    }));
  }

  barColor(percente) {
    if (percente < 33) {
      return 'red';
    }else if (percente < 66) {
      return 'orange';
    }else {
      return 'green';
    }
  }

  calcArmorPercent(armor: number) {
    return Math.round(0.06 * armor / (1 + (0.06 * armor)) * 100);
  }

  heroImg() {
    return 'https://api.opendota.com' + this.hero.img;
  }
}
